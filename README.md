# Tampermonkey - Chrome Scripts

Handy script to append identifying prefix to chrome tabs to better identify localhost vs github etc.

Useful when you have multiple windows open that contain multiple tabs, the active tab in each window will display the prefix.

<img src="tampermonkey-domain-helper.png" width="300" />

## Installation

Install the chrome plugin here: https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo

Create a new tampermonkey script and paste in the contents of this JS file: <a href="https://gitlab.com/maatvee/tampermonkey-chrome-scripts/-/raw/main/chrome-tab_domain-helper.js">chrome-tab_domain-helper.js</a>