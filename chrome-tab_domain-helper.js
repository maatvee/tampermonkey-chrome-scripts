// ==UserScript==
// @name       Chrome Tab Domain Helper
// @namespace  http://use.i.E.your.homepage/
// @version    0.1
// @description  Adds easy to identify prefix to chrome tabs
// @match      *://gitlab.com/*
// @match      **.gitlab.io/*
// @match      *://github.com/*
// @match      *://localhost/*
// @match      *://127.0.0.1/*
// @copyright  2022 Mat Valdman
// ==/UserScript==
(function (document) {
    var wait = 1000;

    setTimeout(function(){
        //helper to extract href
        const getLocation = function(href) {
            var l = document.createElement("a");
            l.href = href;
            return l;
        };

        var hostPrefix = '+++';
        var originalTitle = document.title;
        var hostName = getLocation(document.URL).hostname;

        var hostMap = {
            'maatvee.gitlab.io': '>>>GL: ',
            'gitlab.com':'>>>GL: ',
            'github.com': '>>>GH: ',
            'localhost': '^@@@: ',
            '127.0.0.1': '^@@@: ',
            '0.0.0.0': '^@@@: '
        }

        hostPrefix = hostMap[hostName];
        document.title = hostPrefix + originalTitle;
    },wait);
})(document);